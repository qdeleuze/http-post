﻿namespace http_post
{
    partial class postform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.urlLB = new System.Windows.Forms.Label();
            this.urlTB = new System.Windows.Forms.TextBox();
            this.postGrid = new System.Windows.Forms.DataGridView();
            this.postname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PostData = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addBTN = new System.Windows.Forms.Button();
            this.dltaBTN = new System.Windows.Forms.Button();
            this.postNameTB = new System.Windows.Forms.TextBox();
            this.postDataTB = new System.Windows.Forms.TextBox();
            this.pstnmLB = new System.Windows.Forms.Label();
            this.pstdtLB = new System.Windows.Forms.Label();
            this.startBTN = new System.Windows.Forms.Button();
            this.onTop = new System.Windows.Forms.CheckBox();
            this.dltsBTN = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.resultWB = new System.Windows.Forms.WebBrowser();
            this.bytesLB = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.postGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // urlLB
            // 
            this.urlLB.AutoSize = true;
            this.urlLB.Location = new System.Drawing.Point(12, 9);
            this.urlLB.Name = "urlLB";
            this.urlLB.Size = new System.Drawing.Size(32, 13);
            this.urlLB.TabIndex = 0;
            this.urlLB.Text = "URL:";
            // 
            // urlTB
            // 
            this.urlTB.Location = new System.Drawing.Point(15, 25);
            this.urlTB.Name = "urlTB";
            this.urlTB.Size = new System.Drawing.Size(231, 20);
            this.urlTB.TabIndex = 1;
            this.urlTB.Text = "http://www.skycode.ovh/application/";
            // 
            // postGrid
            // 
            this.postGrid.AllowUserToAddRows = false;
            this.postGrid.AllowUserToDeleteRows = false;
            this.postGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.postGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.postGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.postname,
            this.PostData});
            this.postGrid.Location = new System.Drawing.Point(674, 80);
            this.postGrid.Name = "postGrid";
            this.postGrid.Size = new System.Drawing.Size(246, 383);
            this.postGrid.TabIndex = 2;
            // 
            // postname
            // 
            this.postname.HeaderText = "PostName";
            this.postname.Name = "postname";
            // 
            // PostData
            // 
            this.PostData.HeaderText = "PostData";
            this.PostData.Name = "PostData";
            // 
            // addBTN
            // 
            this.addBTN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.addBTN.Location = new System.Drawing.Point(674, 51);
            this.addBTN.Name = "addBTN";
            this.addBTN.Size = new System.Drawing.Size(74, 20);
            this.addBTN.TabIndex = 4;
            this.addBTN.Text = "Add";
            this.addBTN.UseVisualStyleBackColor = true;
            this.addBTN.Click += new System.EventHandler(this.addBTN_Click);
            // 
            // dltaBTN
            // 
            this.dltaBTN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dltaBTN.Location = new System.Drawing.Point(857, 51);
            this.dltaBTN.Name = "dltaBTN";
            this.dltaBTN.Size = new System.Drawing.Size(63, 20);
            this.dltaBTN.TabIndex = 5;
            this.dltaBTN.Text = "Delete all";
            this.dltaBTN.UseVisualStyleBackColor = true;
            this.dltaBTN.Click += new System.EventHandler(this.dltBTN_Click);
            // 
            // postNameTB
            // 
            this.postNameTB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.postNameTB.Location = new System.Drawing.Point(674, 25);
            this.postNameTB.Name = "postNameTB";
            this.postNameTB.Size = new System.Drawing.Size(116, 20);
            this.postNameTB.TabIndex = 6;
            // 
            // postDataTB
            // 
            this.postDataTB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.postDataTB.Location = new System.Drawing.Point(811, 25);
            this.postDataTB.Name = "postDataTB";
            this.postDataTB.Size = new System.Drawing.Size(109, 20);
            this.postDataTB.TabIndex = 7;
            // 
            // pstnmLB
            // 
            this.pstnmLB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pstnmLB.AutoSize = true;
            this.pstnmLB.Location = new System.Drawing.Point(671, 9);
            this.pstnmLB.Name = "pstnmLB";
            this.pstnmLB.Size = new System.Drawing.Size(62, 13);
            this.pstnmLB.TabIndex = 8;
            this.pstnmLB.Text = "Post Name:";
            // 
            // pstdtLB
            // 
            this.pstdtLB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pstdtLB.AutoSize = true;
            this.pstdtLB.Location = new System.Drawing.Point(808, 9);
            this.pstdtLB.Name = "pstdtLB";
            this.pstdtLB.Size = new System.Drawing.Size(57, 13);
            this.pstdtLB.TabIndex = 9;
            this.pstdtLB.Text = "Post Data:";
            // 
            // startBTN
            // 
            this.startBTN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.startBTN.Location = new System.Drawing.Point(674, 469);
            this.startBTN.Name = "startBTN";
            this.startBTN.Size = new System.Drawing.Size(246, 23);
            this.startBTN.TabIndex = 11;
            this.startBTN.Text = "Launch Post";
            this.startBTN.UseVisualStyleBackColor = true;
            this.startBTN.Click += new System.EventHandler(this.startBTN_Click);
            // 
            // onTop
            // 
            this.onTop.AutoSize = true;
            this.onTop.Location = new System.Drawing.Point(252, 25);
            this.onTop.Name = "onTop";
            this.onTop.Size = new System.Drawing.Size(192, 17);
            this.onTop.TabIndex = 12;
            this.onTop.Text = "Garder la fenetre au dessus de tout";
            this.onTop.UseVisualStyleBackColor = true;
            this.onTop.CheckedChanged += new System.EventHandler(this.onTop_CheckedChanged);
            // 
            // dltsBTN
            // 
            this.dltsBTN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dltsBTN.Location = new System.Drawing.Point(754, 51);
            this.dltsBTN.Name = "dltsBTN";
            this.dltsBTN.Size = new System.Drawing.Size(97, 20);
            this.dltsBTN.TabIndex = 13;
            this.dltsBTN.Text = "Delete(selected)";
            this.dltsBTN.UseVisualStyleBackColor = true;
            this.dltsBTN.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 494);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(325, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Pour mon choupinou d\'Aredissounet qui souffre face à son git bash.";
            // 
            // resultWB
            // 
            this.resultWB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resultWB.Location = new System.Drawing.Point(15, 52);
            this.resultWB.MinimumSize = new System.Drawing.Size(20, 20);
            this.resultWB.Name = "resultWB";
            this.resultWB.Size = new System.Drawing.Size(653, 440);
            this.resultWB.TabIndex = 15;
            // 
            // bytesLB
            // 
            this.bytesLB.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.bytesLB.AutoSize = true;
            this.bytesLB.Location = new System.Drawing.Point(620, 495);
            this.bytesLB.Name = "bytesLB";
            this.bytesLB.Size = new System.Drawing.Size(48, 13);
            this.bytesLB.TabIndex = 16;
            this.bytesLB.Text = "0 Byte(s)";
            this.bytesLB.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.bytesLB.Click += new System.EventHandler(this.bytesLB_Click);
            // 
            // postform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(932, 516);
            this.Controls.Add(this.bytesLB);
            this.Controls.Add(this.resultWB);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dltsBTN);
            this.Controls.Add(this.onTop);
            this.Controls.Add(this.startBTN);
            this.Controls.Add(this.pstdtLB);
            this.Controls.Add(this.pstnmLB);
            this.Controls.Add(this.postDataTB);
            this.Controls.Add(this.postNameTB);
            this.Controls.Add(this.dltaBTN);
            this.Controls.Add(this.addBTN);
            this.Controls.Add(this.postGrid);
            this.Controls.Add(this.urlTB);
            this.Controls.Add(this.urlLB);
            this.Name = "postform";
            this.Text = "HTTP Post (Sky&Diss)";
            ((System.ComponentModel.ISupportInitialize)(this.postGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label urlLB;
        private System.Windows.Forms.TextBox urlTB;
        private System.Windows.Forms.DataGridView postGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn postname;
        private System.Windows.Forms.DataGridViewTextBoxColumn PostData;
        private System.Windows.Forms.Button addBTN;
        private System.Windows.Forms.Button dltaBTN;
        private System.Windows.Forms.TextBox postNameTB;
        private System.Windows.Forms.TextBox postDataTB;
        private System.Windows.Forms.Label pstnmLB;
        private System.Windows.Forms.Label pstdtLB;
        private System.Windows.Forms.Button startBTN;
        private System.Windows.Forms.CheckBox onTop;
        private System.Windows.Forms.Button dltsBTN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.WebBrowser resultWB;
        private System.Windows.Forms.Label bytesLB;
    }
}

