﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;

namespace http_post
{
    public partial class postform : Form
    {
        public postform()
        {
            InitializeComponent();
        }

        private void onTop_CheckedChanged(object sender, EventArgs e)
        {
            if (onTop.Checked)
            {
                this.TopLevel = true;
                this.TopMost = true;
            }
            else
            {
                this.TopMost = false;
            }
        }

        private void addBTN_Click(object sender, EventArgs e)
        {
            string postname;
            string postdata;

            postname = postNameTB.Text;
            postdata = postDataTB.Text;
            this.postGrid.Rows.Add(postname, postdata);
        }

        private void dltBTN_Click(object sender, EventArgs e)
        {
            this.postGrid.RowCount = 0;
            this.postGrid.Rows.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach(DataGridViewRow item in this.postGrid.SelectedRows)
            {
                postGrid.Rows.RemoveAt(item.Index);
            }
        }

        private void startBTN_Click(object sender, EventArgs e)
        {

            System.Collections.Specialized.NameValueCollection formData = new System.Collections.Specialized.NameValueCollection();
            foreach(DataGridViewRow item in postGrid.Rows)
            {
                string _formName = (string)item.Cells[0].Value;
                string _formData = (string)item.Cells[1].Value;

                formData[_formName] = _formData;
            }
            
            try
            {
                WebClient wc = new WebClient();
                Uri URL = new Uri(urlTB.Text);
                string result = Encoding.UTF8.GetString(wc.UploadValues(URL, "POST", formData));

                resultWB.DocumentText = result;
                int inBytes = result.Length * sizeof(Char);
                bytesLB.Text = inBytes + " Byte(s)";
            }
            catch (Exception ex)
            {
                resultWB.DocumentText = "Une erreur est survenue" + Environment.NewLine + ex;
            }
            
        }

        private void bytesLB_Click(object sender, EventArgs e)
        {

        }
    }
}
